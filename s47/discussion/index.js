// console.log('hello, full stack')

// For selecting HTML elements, we'll be using: document.querySelector
// Syntax: document.querySelector("htmlElement");
	/*
		"document" - refers to the whole page
		".querySelector" - used to select specific object/HTML element from the document
	*/
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// [SECTION] Event Listeners
	// Whenever a user interacts with a web page, this action is considered as an event

txtFirstName.addEventListener('keyup', (event) => {

	spanFullName.innerHTML = txtFirstName.value;
})

// Multiple listeners can also be assgined 
txtFirstName.addEventListener('keyup', (event) => {

	// event.targer contains the element where the event happened
	console.log(event.target);

	// event.target.value gets the value of the input
	console.log(event.target.value);
});



//////////////////////////////////
txtLastName.addEventListener('keyup', (event) => {

	spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
})