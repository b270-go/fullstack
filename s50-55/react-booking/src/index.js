import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// Import Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';


//const name = "John Smith"
// const user = {
//   firstName : "Jane",
//   lastName: "Doe"
// }

// function formatName(user){
//   return user.firstName + " " + user.lastName
// }


//const element = <h1> Hello, {name} </h1>

//const element = <h1> Hello, {formatName(user)} </h1>

//createRoot - assigns the element to be managaed by React with its Virtual DOM
const root = ReactDOM.createRoot(document.getElementById('root'));
//root.render (element)

// render() - displays the react elements/components int the root
root.render (
    <React.StrictMode>
        <App />
    </React.StrictMode>
)
